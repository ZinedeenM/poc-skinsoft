package com.example.controller;

import com.example.morphia.todo_list.TodoList;
import com.example.morphia.user.User;
import com.example.morphia.user.UserHelper;
import dev.morphia.Datastore;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/todolist")
public class TodoListController {
    private final Datastore datastore;

    @Autowired
    public TodoListController(Datastore datastore) {
        this.datastore = datastore;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(@RequestParam String token, @RequestParam String name) {
        try {
            User user = UserHelper.checkUser(token, datastore);
            TodoList todoList = new TodoList(user, name);
            datastore.save(todoList);
            UserHelper.addList(user, todoList, datastore);
            return "200";
        }catch (Exception e){
            return "500";
        }
    }

    @RequestMapping(value = "/remove", method = RequestMethod.DELETE)
    public String remove(@RequestParam String token, @RequestParam ObjectId todoList) {
        try {
            User user = UserHelper.checkUser(token, datastore);
            TodoList todoList_queried = datastore.createQuery(TodoList.class).field("id").equal(todoList).first();
            datastore.delete(todoList_queried);
            UserHelper.remList(user, todoList_queried, datastore);
            return "200";
        } catch (Exception e){
            return "500";
        }
    }
}
