package com.example.controller;

import com.example.morphia.user.User;
import dev.morphia.Datastore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController {

    private final Datastore datastore;

    @Autowired
    public UserController(Datastore datastore) {
        this.datastore = datastore;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(@RequestParam String username, @RequestParam String pwd) {
        User new_user = new User("userfailed", "userfailed");
        if (datastore.createQuery(User.class)
                .field("name").equal(username)
                .count() <= 0) {
            new_user = new User(username, pwd);
            this.datastore.save(new_user);
            return "200";
        }else {
            return "500";
        }
    }

    @RequestMapping(value = "/remove", method = RequestMethod.DELETE)
    public String remove(@RequestParam String username, @RequestParam String pwd) {
        try {
            User old_user = datastore.createQuery(User.class)
                    .field("name").equal(username)
                    .field("pwd").equal(pwd).first();

            datastore.delete(old_user);

            return "200";
        } catch (Exception e){
            return "500";
        }
    }
}
