package com.example.controller;

import com.example.morphia.task.Task;
import com.example.morphia.todo_list.TodoList;
import com.example.morphia.todo_list.TodoListHelper;
import com.example.morphia.user.User;
import com.example.morphia.user.UserHelper;
import dev.morphia.Datastore;
import dev.morphia.annotations.Entity;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/task")
public class TaskController {
    private final Datastore datastore;

    @Autowired
    public TaskController(Datastore datastore) {
        this.datastore = datastore;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(@RequestParam String token, @RequestParam ObjectId todoList,
                         @RequestParam String name, @RequestParam Integer position) throws Exception {
        try {
            UserHelper.checkUser(token, datastore);
            TodoList todoList_queried = TodoListHelper.getTodoList(todoList, datastore);
            Task task = new Task(name, position, todoList_queried);
            datastore.save(task);
            TodoListHelper.addTask(todoList_queried, task, datastore);
            return "200";
        } catch (Exception e) {
            throw e;
//            return "500";
        }
    }

    @RequestMapping(value = "/remove", method = RequestMethod.DELETE)
    public String remove(@RequestParam String token, @RequestParam ObjectId todoList,
                         @RequestParam ObjectId task) {
        try {
            UserHelper.checkUser(token, datastore);
            TodoList todoList_queried = TodoListHelper.getTodoList(todoList, datastore);
            Task task_queried = datastore.createQuery(Task.class).field("id").equal(task).first();
            datastore.delete(task_queried);
            TodoListHelper.remTask(todoList_queried, task_queried, datastore);
            return "200";
        } catch (Exception e) {
            return "500";
        }
    }
}
