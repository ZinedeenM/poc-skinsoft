package com.example.controller;

import com.example.App;
import com.example.morphia.user.User;
import com.mongodb.MongoClient;
import dev.morphia.Datastore;
import dev.morphia.Morphia;
import dev.morphia.query.Query;
import dev.morphia.query.UpdateOperations;
import dev.morphia.query.UpdateResults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/")
public class HomeController {

    private final Datastore datastore;

    @Autowired
    public HomeController(Datastore datastore){
        this.datastore = datastore;
    }

    @RequestMapping(value = "/connect")
    public String connect(@RequestParam String username, @RequestParam String pwd){
        try {
            //TODO
            final String connectionToken = "connected";

            User user = datastore.createQuery(User.class)
                    .field("name").equal(username)
                    .field("pwd").equal(pwd).first();

            user.setConnectionToken(connectionToken);

            final Query<User> user_query = datastore.createQuery(User.class).field("id").equal(user.getId());
            final UpdateOperations<User> addToken = datastore.createUpdateOperations(User.class).set("connectionToken", connectionToken);
            datastore.update(user_query, addToken);
            return connectionToken;
        }catch (Exception e){
            return "500";
        }
    }

    @RequestMapping(value = "/disconnect")
    public String disconnect(@RequestParam String token){
        try {
            User user = datastore.createQuery(User.class)
                    .field("connectionToken").equal(token).first();

            user.setConnectionToken(null);

            final Query<User> user_query = datastore.createQuery(User.class).field("id").equal(user.getId());
            final UpdateOperations<User> remToken = datastore.createUpdateOperations(User.class).unset("connectionToken");
            datastore.update(user_query, remToken);
            return "200";
        }catch (Exception e){
            return "500";
        }
    }

}
