package com.example.morphia.task;

import com.example.morphia.todo_list.TodoList;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Reference;
import org.bson.types.ObjectId;

import java.util.List;

@Entity("task")
public class Task {

    @Id
    private ObjectId id;
    private String name;
    private Integer position;
    @Reference
    private TodoList todoList;

    public Task() {

    }

    public Task(String name, Integer position, TodoList todoList) {
        this.name = name;
        this.position = position;
        this.todoList = todoList;
    }
}
