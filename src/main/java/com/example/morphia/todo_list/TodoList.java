package com.example.morphia.todo_list;

import com.example.morphia.task.Task;
import com.example.morphia.user.User;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Reference;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity("todo_list")
public class TodoList {
    @Id
    private ObjectId id;
    private String name;
    @Reference
    private Set<Task> tasks;
    @Reference
    private User user;

    public TodoList(){

    }

    public TodoList(User user, String name){
        this.user = user;
        this.name = name;

        this.tasks = new HashSet<>();
    }

    public ObjectId getId() {
        return id;
    }

    public TodoList addTask(Task task){
        this.tasks.add(task);
        return this;
    }

    public TodoList removeTask(Task task){
        this.tasks.remove(task);
        return this;
    }
}
