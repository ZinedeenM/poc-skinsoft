package com.example.morphia.todo_list;

import com.example.morphia.task.Task;
import com.example.morphia.todo_list.TodoList;
import com.mongodb.internal.operation.UpdateOperation;
import dev.morphia.Datastore;
import dev.morphia.query.Query;
import dev.morphia.query.UpdateOperations;
import dev.morphia.query.UpdateResults;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;

public class TodoListHelper {
    private Datastore datastore;

    @Autowired
    public TodoListHelper(Datastore datastore) {
        this.datastore = datastore;
    }

    public static TodoList getTodoList(ObjectId id, Datastore datastore){
        return datastore.createQuery(TodoList.class).field("id").equal(id).first();
    }

    public static TodoList addTask(TodoList todoList, Task task, Datastore datastore){
        final Query<TodoList> todolist_query = datastore.createQuery(TodoList.class)
                .field("id").equal(todoList.getId());

        final UpdateOperations<TodoList> add = datastore.createUpdateOperations(TodoList.class)
                .addToSet("tasks", task);

        datastore.update(todolist_query, add);

        return todolist_query.first().addTask(task);
    }

    public static TodoList remTask(TodoList todoList, Task task, Datastore datastore){
        final Query<TodoList> todolist_query = datastore.createQuery(TodoList.class)
                .field("id").equal(todoList.getId());

        final UpdateOperations<TodoList> remove = datastore.createUpdateOperations(TodoList.class)
                .removeAll("tasks", task);

        datastore.update(todolist_query, remove);

        return todolist_query.first().removeTask(task);
    }
}
