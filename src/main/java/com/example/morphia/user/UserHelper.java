package com.example.morphia.user;

import com.example.morphia.todo_list.TodoList;
import com.mongodb.internal.operation.UpdateOperation;
import dev.morphia.Datastore;
import dev.morphia.query.Query;
import dev.morphia.query.UpdateOperations;
import dev.morphia.query.UpdateResults;
import org.springframework.beans.factory.annotation.Autowired;

public class UserHelper {
    private Datastore datastore;

    @Autowired
    public UserHelper(Datastore datastore) {
        this.datastore = datastore;
    }

    public static User checkUser(String token, Datastore datastore) throws Exception {
        User user = datastore.createQuery(User.class)
                .field("connectionToken").equal(token).first();

        if (user == null){
            throw new Exception(String.format("Utilisateur non-existent, token = %s", token));
        }
        return user;
    }

    public static User addList(User user, TodoList todoList, Datastore datastore){
        final Query<User> user_query = datastore.createQuery(User.class)
                .field("name").equal(user.getName())
                .field("pwd").equal(user.getPwd());

        final UpdateOperations<User> add = datastore.createUpdateOperations(User.class)
                .addToSet("todoLists", todoList);

        datastore.update(user_query, add);

        return user_query.first().addTodoList(todoList);
    }

    public static User remList(User user, TodoList todoList, Datastore datastore){
        final Query<User> user_query = datastore.createQuery(User.class)
                .field("name").equal(user.getName())
                .field("pwd").equal(user.getPwd());

        final UpdateOperations<User> rem = datastore.createUpdateOperations(User.class)
                .removeAll("todoLists", todoList);

        datastore.update(user_query, rem);

        return user_query.first().removeTodoList(todoList);
    }
}
