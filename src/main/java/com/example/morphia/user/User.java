package com.example.morphia.user;

import com.example.morphia.todo_list.TodoList;
import dev.morphia.annotations.Entity;
import dev.morphia.annotations.Id;
import dev.morphia.annotations.Reference;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity("user")
public class User {

    @Id
    private ObjectId id;
    private String name;
    private String pwd;
    private String connectionToken;

    @Reference
    private Set<TodoList> todoLists;

    public User(){

    }

    public User(String name, String pwd){
        this.name = name;
        this.pwd = pwd;

        this.todoLists = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public String getPwd() {
        return pwd;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public User addTodoList(TodoList todoList) {
        this.todoLists.add(todoList);
        return this;
    }

    public User removeTodoList(TodoList todoList) {
        this.todoLists.remove(todoList);
        return this;
    }

    public void setConnectionToken(String connectionToken) {
        this.connectionToken = connectionToken;
    }

    public ObjectId getId() {
        return id;
    }
}
